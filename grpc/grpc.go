package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/bookshop/bsh_go_catalog_service/config"
	"gitlab.com/bookshop/bsh_go_catalog_service/genproto/catalog_service"
	"gitlab.com/bookshop/bsh_go_catalog_service/grpc/client"
	"gitlab.com/bookshop/bsh_go_catalog_service/grpc/service"
	"gitlab.com/bookshop/bsh_go_catalog_service/pkg/logger"
	"gitlab.com/bookshop/bsh_go_catalog_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	catalog_service.RegisterAuthorServiceServer(grpcServer, service.NewAuthorService(cfg, log, strg, srvc))
	catalog_service.RegisterBookCategoryServiceServer(grpcServer, service.NewBookCategoryService(cfg, log, strg, srvc))
	catalog_service.RegisterBookServiceServer(grpcServer, service.NewBookService(cfg, log, strg, srvc))
	catalog_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
