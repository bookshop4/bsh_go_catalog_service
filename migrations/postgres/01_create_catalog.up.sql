CREATE TABLE IF NOT EXISTS "category"(
    id UUID PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL UNIQUE,
    parent_id UUID REFERENCES category(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

CREATE TABLE  IF NOT EXISTS "author"(
    id UUID PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL DEFAULT 'Unknown author',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "book" (
    id  UUID PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL DEFAULT 'Unknown book',
    author_id UUID REFERENCES author(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "book_category"(
    book_id UUID NOT NULL REFERENCES book(id),
    category_id UUID NOT NULL REFERENCES category(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP
);

-- SELECT
-- 	book.id,
--     book.name AS book_title,
-- 	author.name AS author_name,
-- 	book.created_at,
-- 	book.updated_at
-- FROM book_category
-- JOIN book ON book.id = book_category.book_id
-- JOIN author ON author.id = book.author_id
-- WHERE book_category.deleted_at IS NULL
-- GROUP BY book.id,author.name;

-- SELECT
--     category.id,
--     category.name,
--     category_parent.name AS parent_name,
--     category.created_at,
--     category.updated_at
-- FROM book_category
-- JOIN category ON category.id = book_category.category_id
-- JOIN category AS category_parent ON category_parent.id = category.parent_id
-- WHERE book_category.book_id = '07c71893-b0ed-42e3-a9f1-bd8e098b6f8e' AND book_category.deleted_at IS NULL;